solution "gmsv_mongof"
	configurations { "Release" }
	location ( "projects/" .. os.target() .. "-" .. _ACTION)

	project "gmsv_mongof"
		kind "SharedLib"
		architecture "x86"
		language "C++"
		cppdialect "C++17"

		editandcontinue "Off"
		vectorextensions "SSE"
		symbols "Off"
		flags { "NoPCH", "NoImportLib" }

		includedirs {
			"include/gmod",
			"include/" .. os.target() .. "/libbson-1.0",
			"include/" .. os.target() .. "/libmongoc-1.0",
		}

		libdirs { 
			"lib/", -- windows-only
		}

		links {
			"mongoc-1.0",
			"bson-1.0",
		}

		targetdir "build"
		
		files {
			"src/**.*"
		}

		targetprefix ""
		if os.istarget( "windows" ) then 
			targetsuffix("_win32")
		end
		if os.istarget( "linux" ) 	then 
			targetsuffix("_linux")
		end
		targetextension ".dll"


		configurations "Release"
			optimize "Speed"