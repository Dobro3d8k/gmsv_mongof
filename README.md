# gmsv_mongof 
[![Documentation Status](https://readthedocs.org/projects/gmsv-mongof/badge/?version=latest)](http://gmsv-mongof.readthedocs.io/en/latest/?badge=latest)

gmsv_mongof is a binary module for Garry's Mod. 
It gives the interface between Garry's Mod lua and libmongoc

## Issues / Proposals
If you have found a bug or have a beautiful idea for new feature then create new issue:
 - [Issue Tracker](https://bitbucket.org/kelni/gmsv_mongof/issues)

## Communication
If you are having any module-related trouble  or question come join our [Discord](https://discord.gg/zUGcAD4)

## License

The project is licensed under MIT.
