#ifndef COLLECTION_HPP
#define COLLECTION_HPP

#include "Module.hpp"

extern int CollectionMetaTableId;

int CollectionCommandSimple(lua_State * L);
int CollectionFind(lua_State * L);
int CollectionInsert(lua_State * L);
int CollectionRemove(lua_State * L);
int CollectionUpdate(lua_State * L);
int DestroyCollection(lua_State * L);

#endif // !COLLECTION_HPP

