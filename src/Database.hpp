#ifndef DATABASE_HPP
#define DATABASE_HPP

#include "Module.hpp"

extern int DatabaseMetaTableId;

int DestroyDatabase(lua_State * L);
int DatabaseAddUser(lua_State * L);
int DatabaseCommandSimple(lua_State * L);
int DatabaseCopy(lua_State * L);
int DatabaseCreateCollection(lua_State * L);
int DatabaseDrop(lua_State * L);
int DatabaseGetCollection(lua_State * L);
int DatabaseGetName(lua_State * L);
int DatabaseHasCollection(lua_State * L);
int DatabaseRemoveUser(lua_State * L);

#endif // !DATABASE_HPP
