#include "Database.hpp"

int DestroyDatabase(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;
	
	mongoc_database_destroy(database);

	return 0;
}

int DatabaseAddUser(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	const char * username = LUA->CheckString(2);
	const char * password = LUA->CheckString(3);

	int rolesRef = INT_MIN, dataRef = INT_MIN;

	if (LUA->Top() == 5) {
		if (LUA->IsType(4, Type::TABLE) && LUA->IsType(5, Type::TABLE)) {
			dataRef = LUA->ReferenceCreate();
			rolesRef = LUA->ReferenceCreate();	
		} else if (LUA->IsType(5, Type::TABLE)) {
			dataRef = LUA->ReferenceCreate();
		} else {
			LUA->ThrowError("given wrong number of arguments");
		}
	} else if (LUA->Top() == 4 && LUA->IsType(4, Type::TABLE)) {
			rolesRef = LUA->ReferenceCreate();
	}

	bson_t * roles;
	if (rolesRef != INT_MIN) {
		roles = MongoFUtility::LuaToBSON(L, rolesRef);
		LUA->ReferenceFree(rolesRef);
	}

	bson_t * custom_data;
	if (dataRef != INT_MIN) {
		custom_data = MongoFUtility::LuaToBSON(L, dataRef);
	}

	bson_error_t * error;
	bool success = mongoc_database_add_user(database, username, password, roles, custom_data, error);
	
	if (rolesRef != INT_MIN) {
		LUA->ReferenceFree(rolesRef);
		bson_destroy(roles);
	}

	if (dataRef != INT_MIN) {
		LUA->ReferenceFree(dataRef);
		bson_destroy(custom_data);
	}

	LUA->PushBool(success);

	return 1;
}

int DatabaseCommandSimple(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	LUA->CheckType(2, Type::TABLE);
	int commandRef = LUA->ReferenceCreate();
	bson_t * command = MongoFUtility::LuaToBSON(L, commandRef);
	LUA->ReferenceFree(commandRef);

	bson_t reply;
	bson_error_t * error;
	bool success = mongoc_database_command_simple(database, command, NULL, &reply, error);
	if (!success) {
		LUA->ThrowError(error->message);
		return 0;
	}
	bson_destroy(command);
	
	int resultRef = MongoFUtility::BSONToLua(L, &reply);

	LUA->ReferencePush(resultRef);

	return 1;
}

int DatabaseCopy(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	mongoc_database_t * databaseCopy = mongoc_database_copy(database);
	
	LUA->PushUserType(databaseCopy, DatabaseMetaTableId);

	return 1;
}

int DatabaseCreateCollection(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	const char * name = LUA->CheckString(2);
	
	bson_t * options;
	if (LUA->IsType(3, Type::TABLE)) {
		int optionsRef = LUA->ReferenceCreate();
		options = MongoFUtility::LuaToBSON(L, optionsRef);
	}

	bson_error_t * error;
	mongoc_collection_t * collection = mongoc_database_create_collection(database, name, options, error);

	LUA->PushUserType(collection, CollectionMetaTableId);

	return 1;
}

int DatabaseDrop(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	bson_error_t * error;
	bool success = mongoc_database_drop(database, error);
	if (!success && error != NULL && error->code != 0) {
		LUA->ThrowError(error->message);
	}

	LUA->PushBool(success);

	return 1;
}

int DatabaseGetCollection(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	const char * name = LUA->CheckString(2);
	
	mongoc_collection_t * collection = mongoc_database_get_collection(database, name);

	LUA->PushUserType(collection, CollectionMetaTableId);

	return 1;	
}

int DatabaseGetName(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;
	
	const char * name = mongoc_database_get_name(database);

	LUA->PushString(name);

	return 1;	
}

int DatabaseHasCollection(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	const char * name = LUA->CheckString(2);

	bson_error_t * error;
	bool hasCollection = mongoc_database_has_collection(database, name, error);
	if (error->code != 0) {
		LUA->ThrowError(error->message);
		return 0;
	}

	LUA->PushBool(hasCollection);

	return 1;	
}

int DatabaseRemoveUser(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_database_t * database = LUA->GetUserType<mongoc_database_t>(1, DatabaseMetaTableId);
	if (database == NULL) return 0;

	const char * username = LUA->CheckString(2);

	bson_error_t * error;
	bool success = mongoc_database_remove_user(database, username, error);
	if (error->code != 0) {
		LUA->ThrowError(error->message);
		return 0;
	}

	LUA->PushBool(success);

	return 1;	
}
