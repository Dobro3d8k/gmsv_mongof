#include "Module.hpp"

int ClientMetaTableId, DatabaseMetaTableId, CollectionMetaTableId;

GMOD_MODULE_OPEN() {
	mongoc_init();

	ClientMetaTableId = LUA->CreateMetaTable("MongoFClient");
	
		LUA->Push(-1);
		LUA->SetField(-2, "__index");

		LUA->PushCFunction(DestroyClient);
		LUA->SetField(-2, "__gc");

		LUA->PushCFunction(ClientCommand);
		LUA->SetField(-2, "Command");

		LUA->PushCFunction(ClientUri);
		LUA->SetField(-2, "Uri");

		LUA->PushCFunction(ClientListDatabases);
		LUA->SetField(-2, "ListDatabases");

		LUA->PushCFunction(ClientDatabase);
		LUA->SetField(-2, "Database");
		
		LUA->PushCFunction(ClientCollection);
		LUA->SetField(-2, "Collection");

	LUA->Pop();

	DatabaseMetaTableId = LUA->CreateMetaTable("MongoFDatabase");

		LUA->Push(-1);
		LUA->SetField(-2, "__index");

		LUA->PushCFunction(DestroyDatabase);
		LUA->SetField(-2, "__gc");

		LUA->PushCFunction(DatabaseAddUser);
		LUA->SetField(-2, "AddUser");

		LUA->PushCFunction(DatabaseCommandSimple);
		LUA->SetField(-2, "CommandSimple");

		LUA->PushCFunction(DatabaseCopy);
		LUA->SetField(-2, "Copy");

		LUA->PushCFunction(DatabaseCreateCollection);
		LUA->SetField(-2, "CreateCollection");

		LUA->PushCFunction(DatabaseDrop);
		LUA->SetField(-2, "Drop");

		LUA->PushCFunction(DatabaseGetCollection);
		LUA->SetField(-2, "GetCollection");

		LUA->PushCFunction(DatabaseGetName);
		LUA->SetField(-2, "GetName");

		LUA->PushCFunction(DatabaseHasCollection);
		LUA->SetField(-2, "HasCollection");

		LUA->PushCFunction(DatabaseRemoveUser);
		LUA->SetField(-2, "RemoveUser");


	LUA->Pop();

	CollectionMetaTableId = LUA->CreateMetaTable("MongoFCollection");

		LUA->Push(-1);
		LUA->SetField(-2, "__index");

		LUA->PushCFunction(DestroyClient);
		LUA->SetField(-2, "__gc");

		LUA->PushCFunction(CollectionCommandSimple);
		LUA->SetField(-2, "CommandSimple");

		LUA->PushCFunction(CollectionFind);
		LUA->SetField(-2, "Find");

		LUA->PushCFunction(CollectionInsert);
		LUA->SetField(-2, "Insert");

		LUA->PushCFunction(CollectionRemove);
		LUA->SetField(-2, "Remove");

		LUA->PushCFunction(CollectionUpdate);
		LUA->SetField(-2, "Update");

	LUA->Pop();

	LUA->PushSpecial(SPECIAL_GLOB);
	LUA->PushString("mongof");
	LUA->PushCFunction(NewClient);
	LUA->SetTable(-3);


	return 0;
}

GMOD_MODULE_CLOSE() {
	mongoc_cleanup();
    return 0;
}