#include "Client.hpp"

int NewClient(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	const char * connectionURI = LUA->CheckString(1);
	mongoc_client_t * client = mongoc_client_new(connectionURI);
	
	bson_error_t error;
	bool r = mongoc_client_command_simple(client, "admin", BCON_NEW("ping", BCON_INT32(1)), NULL, NULL, &error);
	
	if (!r) {
		LUA->ThrowError(error.message);
		return 0;
	}
	LUA->PushUserType(client, ClientMetaTableId);
	return 1;
}

int DestroyClient(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * client = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (client == NULL) return 0;
	mongoc_client_destroy(client);
	return 0;
}

int ClientCommand(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * client = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (client == NULL) return 0;
	
	const char * dbName = LUA->CheckString(2);
	LUA->CheckType(3, Type::TABLE);
	int cmdRef = LUA->ReferenceCreate();

	bson_t * command = MongoFUtility::LuaToBSON(L, cmdRef);
	bson_t reply;
	bson_error_t * error;

	bool success = mongoc_client_command_simple(client, dbName, command, NULL, &reply, error);
	bson_destroy(command);
	if (!success) {
		LUA->ThrowError(error->message);
		bson_destroy(&reply);
		return 0;
	}

	int rplyRef = MongoFUtility::BSONToLua(L, &reply);
	bson_destroy(&reply);
	LUA->ReferencePush(rplyRef);
	return 1;
}

int ClientUri(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * client = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (client == NULL) return 0;
	const mongoc_uri_t * uri = mongoc_client_get_uri(client);
	LUA->PushString(mongoc_uri_get_string(uri));
	return 1;
}

int ClientDatabase(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * client = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (client == NULL) return 0;
	
	const char * dbName = LUA->CheckString(2);
	mongoc_database_t * db = mongoc_client_get_database(client, dbName);

	bson_error_t error;
	bool r = mongoc_database_command_simple(db, BCON_NEW("ping", BCON_INT32(1)), NULL, NULL, &error);
	if (!r) {
		LUA->ThrowError(error.message);
		return 0;
	}
	LUA->PushUserType(db, DatabaseMetaTableId);
	return 1;
}

int ClientListDatabases(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * c = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (c == NULL) return 0;
	
	mongoc_cursor_t * cursor = mongoc_client_find_databases_with_opts(c, NULL);
	
	const bson_t * bson;
	LUA->CreateTable();
	int resultTable = LUA->ReferenceCreate();
	int j = 0;
	while (mongoc_cursor_next(cursor, &bson)) {
		LUA->ReferencePush(resultTable);
			LUA->PushNumber(j);
			LUA->ReferencePush(MongoFUtility::BSONToLua(L, bson));
		LUA->SetTable(-3);
		
		j++;
	}
	mongoc_cursor_destroy(cursor);
	
	LUA->ReferencePush(resultTable);
	return 1;
}

int ClientCollection(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_client_t * client = LUA->GetUserType<mongoc_client_t>(1, ClientMetaTableId);
	if (client == NULL) return 0;

	const char * dbName = LUA->CheckString(2), 
			* collName = LUA->CheckString(3);

	mongoc_collection_t * collection = mongoc_client_get_collection(client, dbName, collName);

	LUA->PushUserType(collection, CollectionMetaTableId);
	return 1;
}
