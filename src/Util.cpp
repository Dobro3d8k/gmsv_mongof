#include "Util.hpp"

int MongoFUtility::BSONToLua(lua_State * L, const bson_t * bson) {
	ILuaBase * LUA = L->luabase;

	bson_iter_t iter;

	LUA->CreateTable();
	if (bson_iter_init(&iter, bson)) {
		while(bson_iter_next(&iter)) {
			bson_type_t t = bson_iter_type(&iter);
			if (t == BSON_TYPE_DOUBLE) {
				LUA->PushNumber(bson_iter_as_double(&iter));	
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_INT32 || t == BSON_TYPE_INT64) {
				LUA->PushNumber((double)bson_iter_as_int64(&iter));
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_BOOL) {
				LUA->PushBool(bson_iter_bool(&iter));	
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_UTF8) {
				LUA->PushString(bson_iter_utf8(&iter, NULL));	
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_DOCUMENT) {
				uint32_t len;
				const uint8_t * data;
				bson_t bson;
				bson_iter_document(&iter, &len, &data);
				bson_init_static(&bson, data, (size_t)len);
				LUA->ReferencePush(MongoFUtility::BSONToLua(L, &bson));
				bson_destroy(&bson);
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_ARRAY) {
				uint32_t len;
				const uint8_t * data;
				bson_t bson;
				bson_iter_array(&iter, &len, &data);
				bson_init_static(&bson, data, len);
				LUA->ReferencePush(MongoFUtility::BSONToLua(L, &bson));
				bson_destroy(&bson);
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_OID) {
				const bson_oid_t * oid = bson_iter_oid(&iter);
				char str[25];
				bson_oid_to_string(oid, str);
				LUA->PushString(str);
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_DATE_TIME) {
				LUA->PushNumber((double)bson_iter_date_time(&iter));
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_REGEX) {
				LUA->PushString(bson_iter_regex(&iter, NULL));
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_CODE) {	
				LUA->PushString(bson_iter_code(&iter, NULL));
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_TIMESTAMP) {
				uint32_t t;
				bson_iter_timestamp (&iter, &t, NULL);
				LUA->PushNumber(t);
				LUA->SetField(-2, bson_iter_key(&iter));
			} else if (t == BSON_TYPE_NULL) {
				LUA->PushNil();	
				LUA->SetField(-2, bson_iter_key(&iter));
			}
			
		}
	}
	return LUA->ReferenceCreate();
}

const char * MongoFUtility::LuaToJSON(lua_State * L, int tableRef) {
	ILuaBase * LUA = L->luabase;
	const char * json;
	LUA->ReferencePush(tableRef);

	LUA->PushSpecial(SPECIAL_GLOB);
		LUA->GetField(-1, "util");
			LUA->GetField(-1, "TableToJSON");
				LUA->ReferencePush(tableRef);
				if (LUA->PCall(1, 1, 0) != 0)
					LUA->ThrowError(LUA->GetString(-1));
				if (!LUA->IsType(-1, Type::STRING)) {
					return NULL;
				}
				json = LUA->GetString(-1);
		LUA->Pop(2);
	LUA->ReferenceFree(tableRef);

	return json;
}

bson_t * MongoFUtility::LuaToBSON(lua_State * L, int tableRef) {
	const char * json = MongoFUtility::LuaToJSON(L, tableRef);

	bson_error_t error;
	bson_t * bson = bson_new_from_json((const uint8_t *) json, -1, &error);
	if (error.code != 0) {
		L->luabase->ThrowError(error.message);
		return NULL;
	}

	return bson;
}