#ifndef MODULE_HPP
#define MODULE_HPP

#include <mongoc.h>

#include <GarrysMod/Lua/Interface.h>

#include "Util.hpp"
#include "Client.hpp"
#include "Database.hpp"
#include "Collection.hpp"

using namespace GarrysMod::Lua;

#endif // !MODULE_HPP