#include "Collection.hpp"

int CollectionCommandSimple(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;

	LUA->CheckType(2, Type::TABLE);
	int commandRef = LUA->ReferenceCreate();

	bson_t * command = MongoFUtility::LuaToBSON(L, commandRef);

	bson_t reply;
	bson_error_t * error;
	bool success = mongoc_collection_command_simple(collection, command, NULL, &reply, error);

	bson_destroy(command);

	if (success) {
		LUA->ReferencePush(MongoFUtility::BSONToLua(L, &reply));
	} else {
		LUA->PushBool(success);
	}

	bson_destroy(&reply);

	return 1;
}

int CollectionFind(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;
	
	int queryRef = INT_MIN, optsRef = INT_MIN;

	if (LUA->Top() == 3) {
		LUA->CheckType(2, Type::TABLE);
		LUA->CheckType(3, Type::TABLE);
		optsRef = LUA->ReferenceCreate();
		queryRef = LUA->ReferenceCreate();
	} else if (LUA->Top() == 2) {
		LUA->CheckType(2, Type::TABLE);
		queryRef = LUA->ReferenceCreate();
	} else {
		LUA->ThrowError("find: given wrong number of arguments");
	}

	bson_t * query = MongoFUtility::LuaToBSON(L, queryRef);
	if (query == NULL) {
		LUA->ThrowError("Failed to build query");
		return 0;
	}

	bson_t * opts;

	mongoc_cursor_t * cursor;

	if (optsRef != INT_MIN) {
		opts = MongoFUtility::LuaToBSON(L, optsRef);
		cursor = mongoc_collection_find_with_opts(collection, query, opts, mongoc_read_prefs_new(MONGOC_READ_PRIMARY));
	} else {
		cursor = mongoc_collection_find_with_opts(collection, query, NULL, mongoc_read_prefs_new(MONGOC_READ_PRIMARY));
	}
	bson_destroy(query);
	if (optsRef != INT_MIN) {
		bson_destroy(opts);
	}


	const bson_t * bson;
	LUA->CreateTable();
	int resultTable = LUA->ReferenceCreate();
	int j = 0;
	while (mongoc_cursor_next(cursor, &bson)) {
		LUA->ReferencePush(resultTable);
			LUA->PushNumber(j);
			LUA->ReferencePush(MongoFUtility::BSONToLua(L, bson));
		LUA->SetTable(-3);
		
		j++;
	}
	mongoc_cursor_destroy(cursor);
	LUA->ReferencePush(resultTable);
	return 1;
}

int CollectionInsert(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;

	LUA->CheckType(2, Type::TABLE);
	int docRef = LUA->ReferenceCreate();
	bson_t * doc = MongoFUtility::LuaToBSON(L, docRef);

	bool success = mongoc_collection_insert(collection, MONGOC_INSERT_NONE, doc, NULL, NULL);
	bson_destroy(doc);

	LUA->PushBool(true);
	return 1;
}

int CollectionRemove(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;

	LUA->CheckType(2, Type::TABLE);
	int selRef = LUA->ReferenceCreate();
	bson_t * selector = MongoFUtility::LuaToBSON(L, selRef);
	
	bool success = mongoc_collection_remove (collection, MONGOC_REMOVE_NONE, selector, NULL, NULL);
	bson_destroy(selector);

	LUA->PushBool(success);
	return 1;
}

int CollectionUpdate(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;

	LUA->CheckType(3, Type::TABLE);
	LUA->CheckType(2, Type::TABLE);
	int updRef = LUA->ReferenceCreate();
	int selRef = LUA->ReferenceCreate();
	bson_t * selector = MongoFUtility::LuaToBSON(L, selRef);
	bson_t * update = MongoFUtility::LuaToBSON(L, updRef);

	bool success = mongoc_collection_update (collection, MONGOC_UPDATE_MULTI_UPDATE, selector, update, NULL, NULL);
	bson_destroy(selector);
	bson_destroy(update);

	LUA->PushBool(success);
	return 1;
}

int DestroyCollection(lua_State * L) {
	ILuaBase * LUA = L->luabase;
	mongoc_collection_t * collection = LUA->GetUserType<mongoc_collection_t>(1, CollectionMetaTableId);
	if (collection == NULL) return 0;
	mongoc_collection_destroy(collection);
	return 0;
}