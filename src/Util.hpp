#ifndef UTIL_HPP
#define UTIL_HPP

#include "Module.hpp"

namespace MongoFUtility {
	int BSONToLua(lua_State * L, const bson_t * bson);
	const char * LuaToJSON(lua_State * L, int tableRef);
	bson_t * LuaToBSON(lua_State * L, int tableRef);
	
}

#endif // !UTIL_HPP
