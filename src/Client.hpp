#ifndef CLIENT_HPP
#define CLIENT_HPP

#include "Module.hpp"

int NewClient(lua_State * L);
int DestroyClient(lua_State * L);
int ClientCommand(lua_State * L);
int ClientUri(lua_State * L);
int ClientListDatabases(lua_State * L);
int ClientDatabase(lua_State * L);
int ClientCollection(lua_State * L);

extern int ClientMetaTableId;

#endif // !CLIENT_HPP
