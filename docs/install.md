# Installing the gmsv_mongof

## Install for usage

### Ubuntu Linux

1. Install dependencies with the following command:
```Shell
sudo apt-get install libmongoc-dev:i386
```
2. Copy gmsv_mongof_linux.dll to garrysmod/lua/bin directory

## Install to build or to contribute

1. Install libraries that needed to compile library with the following command:
```Shell
sudo apt-get install build-essential gcc-multilib g++-multilib libmongoc-dev:i386
```
2. Clone github repository
3. Run BuildProjects.sh
4. `Make` the project inside `projects/linx-gmake`. Library will be built into `build` directory