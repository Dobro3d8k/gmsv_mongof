# gmsv_mongof GMod binary module
libmongoc interface for Garry's Mod

## Introduction
The gmsv_mongof is a server-side binary module for using MongoDB from Lua in Garry's Mod.

It depends on [libmongoc](https://github.com/mongodb/mongo-c-driver) and [libbson](https://github.com/mongodb/libbson).
https://github.com/mongodb/libbson

- [Installation](./install.md)
- [Tutorial](./tutorial.md)
- Metatables
    - [MongoFClient](./client.md)
    - [MongoFDatabase](./database.md)
    - [MongoFCollection](./collection.md)