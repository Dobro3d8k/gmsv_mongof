# MongoFClient
MongoDB connection

## Functions
### Command
```Lua
table 
Command (MongoFClient client,
         string databaseName, 
         table command);
```
Executes `command` on `databaseName` and returns table of results for command
string 

### Uri
```Lua
string
Uri (MongoFClient client);
```

Returns the connection Uri of `client`


### ListDatabases
```Lua
table
ListDatabases (MongoFClient client);
```
Returns table of Databases info of `client`

### Database
```Lua
MongoFDatabase
Database (MongoFClient client,
          string databaseName);
```
Returns database from `client` with name `databaseName` 

### Collection
```Lua
MongoFDatabase
Collection (MongoFClient client,
            string databaseName,
            string collectionName);
```
Returns collection from `client`, `databaseName` with name `collectionName`

## Example

```Lua
require("mongof");
local client = mongof("mongodb://127.0.0.1:27017");
PrintTable(client:ListDatabases());
```