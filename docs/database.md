# MongoFDatabase
MongoDB database

## Functions
### AddUser
```Lua
bool
AddUser (MongoFDatabase database,
         string username,
         string password,
         optional table roles,
         optional table data);
```
Creates user in `database` with given username and password, roles if `roles` is given. Extra user data can be passed through `data`. Returns true if successful, false otherwise.

### CommandSimple
```Lua
table
CommandSimple (MongoFDatabase database,
               table command);
```
Executes `command` on `database`. Return table of results

### Copy
Useless at the time
### CreateCollection
```Lua
MongoFCollection
CreateCollection (MongoFDatabase database,
                  string collectionName,
                  optional table options);
```

Create collection with `collectionName` in `database` with the given `options`. Returns collection.

### Drop
```Lua
bool
Drop (MongoFDatabase database);
```
Tries to drop `database` returns true if successful, false otherwise. 

### GetCollection
```Lua
MongoFCollection
GetCollection (MongoFDatabase database,
               string collectionName);
```
Returns collection with the given `collectionName` from `database`

### GetName
```Lua
string
GetName (MongoFDatabase database);
```
Returns name of given `database`

### HasCollection
```Lua
bool
HasCollection (MongoFDatabase database,
               string collectionName);
```
Returns true if given `database` has collection with given `collectionName`, false otherwise.

### RemoveUser
```Lua
bool
RemoveUser (MongoFDatabase database,
               string username);
```
Tries to remove user with given `username` from database returns true if successful, false otherwise.


## Example

```Lua
require("mongof");
local client = mongof("mongodb://127.0.0.1:27017");
local database = client:Database("myDatabase");
PrintTable(database:CommandSimple({
    ping = 1,
}));
```
