# Tutorial

## Installing

Refer to the [Installation](./install.md) instructions

## Require module

As any other GMod binary modules, gmsv_mongof need to be required in your code

```Lua
require("mongof");
```
Create the connection
```Lua
local client = mongof("mongodb://127.0.0.1:27017");
```
Select the database
```Lua
local database = client:Database("mydatabase");
```
Select the collections
```Lua
local collection = database:GetCollection("superCollection");
```
Perform your actions
```Lua
collection:Insert({
    mongof = "perfect",
    version = 1.0,
    greek_gods = {
        "Aphrodite",
        "Apollo",
        "Ares",
        "Zeus",
    },
});