# MongoFCollection
MongoDB collection

## Functions
### CommandSimple
```Lua
bool/table
CommandSimple (MongoFCollection collection,
               table command);
```
Executes `command` on `collection`. Returns table of results if successful, false otherwise.

### Find
```Lua
table
Find (MongoFCollection collection,
      table query,
      optional table options);
```
Finds all documents in `collection` corresponding to the `query` executed with given `options`. Return table of results.

### Insert
```Lua
bool
Insert (MongoFCollection collection,
        table document);
```
Inserts `document` into `collection`. Returns true if successful, false otherwise.

### Remove
```Lua
bool
Remove (MongoFCollection collection,
        table selector);
```
Removes documents corresponding to `selector` from `collection`. Returns true if successful, false otherwise.

### Update
```Lua
bool
Update (MongoFCollection collection,
        table selector,
        table update)
```
Updates documents that corresponds `selector` in `collection` by the given `update` rules. Returns true if successful, false otherwise.

## Example

```Lua
require("mongof");
local client = mongof("mongodb://127.0.0.1:27017");
local database = client:Database("myDatabase");
local collection = database:GetCollection("myCollection");
collection:Insert({
    kelniCool = true,
    superModule = "mongof",
    gods = {
        "Zeus",
        "Ares",
    }
})
```
